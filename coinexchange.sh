#!/bin/bash

#set -x
#set -v

help() {
        cat <<EOF | expand -t 4
- Coinexchange -
Get cryptocurrency latest rates and calculate current profit or loss

    Usage: bash $(basename "${0}") <currency> [<balance-file> | <total_coin> <total_paid>]

    Examples:
        bash $(basename "${0}") BTC
        bash $(basename "${0}") LTC 2.45 409
        bash $(basename "${0}") BTC /path/to/balance

    Balance file is a plain-text 2-columns table:

    # comments are ignored
    #coins/wallet                           paid
    1.23                                    80
    @1pGYZFBQdEVigDJT5PpGEmwcklEwqcvq29
    @1a81bl4aTC5yFBztMIGHXmnKWZRJGRRSrk     150

EOF
}

jparse() {
    jq -r "${2}" <<<"${1}"
}

jparseround() {
    jq -r "${2}" <<<"${1}" | awk '{printf("%.2f",$1)}'
}


if [[ "${#}" -lt 1 ]] ; then
    help
    exit 1
fi

fiat="EUR"
curr="${1}"

blkchurl="https://chain.so/api/v2/address/${curr}"

if [[ -f "${2}" ]] ; then
    file="${2}"
    readarray -t addrs <<<"$( awk '/@/{print $1}' "${file}" | sed 's/@//g' )"
    addrcoins=0
    for addr in "${addrs[@]}" ; do
        addrdata=$( curl "${blkchurl}/${addr}" -s -L )
        balance=$( jparse "${addrdata}" .data.balance )
        addrcoins=$( awk -v ba="${balance}" -v ac="${addrcoins}" 'BEGIN{printf(ba + ac)}' )
    done
    plaincoins=$( grep -v -e '^#' -e '@' "${file}" | awk '{ sum += $1 } END { print sum }' )
    coins=$( awk -v pc="${plaincoins}" -v ac="${addrcoins}" 'BEGIN{printf("%.7f",pc + ac)}' )
else
    coins=$( awk -v co="${2}" 'BEGIN{printf("%.7f",co)}' )
fi

if [[ -f "${2}" ]] ; then
    sum=$( grep -v -e '^#' "${2}" | awk '{ sum += $2 } END { print sum }' )
    paid=$( awk -v pa="${sum}" 'BEGIN{printf("%.2f",pa)}' )
else
    paid=$( awk -v pa="${3}" 'BEGIN{printf("%.2f",pa)}' )
fi

exurl="https://apiv2.bitcoinaverage.com/indices/global/ticker/${curr}${fiat}"
data=$( curl "${exurl}" -s -L )

timest=$( jparse "${data}" .display_timestamp )
last=$( jparseround "${data}" .last )
aday=$( jparseround "${data}" .averages.day )
aweek=$( jparseround "${data}" .averages.week )
amonth=$( jparseround "${data}" .averages.month )
cday=$( jparseround "${data}" .changes.percent.day )
cweek=$( jparseround "${data}" .changes.percent.week )
cmonth=$( jparseround "${data}" .changes.percent.month )
cyear=$( jparseround "${data}" .changes.percent.year )

dlast=$( awk -v la="${last}" -v mo="${amonth}" 'BEGIN{printf("%.2f",(la / mo) - 1)}' )
dday=$( awk -v da="${aday}" -v mo="${amonth}" 'BEGIN{printf("%.2f",(da / mo) - 1)}' )
dweek=$( awk -v we="${aweek}" -v mo="${amonth}" 'BEGIN{printf("%.2f",(we / mo) - 1)}' )


if [[ -n ${coins} ]] ; then
    asset=$( awk -v co="${coins}" -v ch="${last}" 'BEGIN{printf("%.2f",co * ch)}' )
fi

if [[ -n ${paid} ]] ; then
    gailo=$( awk -v as="${asset}" -v pa="${paid}" 'BEGIN{printf("%.2f",as - pa)}' )
    if [[ "${coins}" == "0.0000000" ]] ; then
        paidr="_"
    else
        paidr=$( awk -v pa="${paid}" -v co="${coins}" 'BEGIN{printf("%.2f",pa / co)}' )
    fi
fi

if [[ "${gailo/.*/}" -lt 0 ]] ; then
    verdict="loss_:("
else
    verdict="profit_:)"
fi

echo "== ${timest} =="
{
cat <<EOF | expand -t 4
${fiat}/${curr} ${last}     LAST
${fiat}/${curr} ${aday},${aweek},${amonth} average_(day,week,month)
%               ${cday},${cweek},${cmonth},${cyear} change_last/open(day,week,month,year)
%               ${dlast},${dday},${dweek} change_last,avg(day,week)/avg(month)
EOF
if [[ -n "${paid}" ]] ; then
cat <<EOF | expand -t 4
_
${curr}         ${coins}    in_wallets
${fiat}         ${paid}     paid
${fiat}/${curr} ${paidr}    paid
${fiat}         ${asset}    current_value
${fiat}         ${gailo}    ${verdict}
EOF
fi
} | column -t | sed 's/_/ /g'
